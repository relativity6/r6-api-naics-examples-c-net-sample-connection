﻿using System;
using System.Net.Http;
using System.Json;
using System.Text;
using System.Threading.Tasks;

public class Program
{
    private static readonly HttpClient client = new HttpClient();

    public static async Task  Main()
    {
        
        var myObject = new JsonObject();
        
        myObject.Add("name", "Relativity6");
        myObject.Add("token", "YOUR_TOKEN_HERE");
        
        var content = new StringContent(myObject.ToString(), Encoding.UTF8, "application/json");

        try
        {
            var response = await client.PostAsync("https://apiv2.usemarvin.ai/naics/naics/search/additional", content);
            
            var responseString = await response.Content.ReadAsStringAsync();
	
            Console.WriteLine(responseString);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }

    }
	
	
}